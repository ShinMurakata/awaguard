// gulpプラグインの読み込み
var gulp = require("gulp");
//fsファイルの読み込み
var fs = require( 'fs' );
// HTMLファイルリネームプラグインの読み込み
var rename = require("gulp-rename");
// Sassをコンパイルするプラグインの読み込み
var sass = require("gulp-sass");
// 「gulp-sass-glob」を読み込む
var sassGlob = require('gulp-sass-glob');
//ソースマップ出力プラグインの読み込み
var sourcemaps = require('gulp-sourcemaps');
//ブラウザシンクのプラグイン読み込み
var browserSync = require('browser-sync').create();
//Sass記述エラー時にwatuchタスクを止めないプラグインの読み込み
var plumber = require('gulp-plumber');
//エラー通知プラグインの読み込み
var notify = require('gulp-notify');
//テンプレートエンジンプラグインの読み込み
var ejs = require('gulp-ejs');
//文字列置換プラグインの読み込み
var replace = require('gulp-replace');
//乱数生成プラグインの読み込み
var crypto = require('crypto');
//lキャッシュ対策乱数生成
var revision = crypto.randomBytes(8).toString('hex');
//タイムスタンプ更新プラグイン読み込み
var through2 = require('through2');


//gulp-sassの設定
gulp.task('sass', function(){
  return gulp
    .src("develop/scss/**/**.scss")
    // plumberの引数にエラーメッセージを設定
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    // gulp.srcの直後に指定
    //.pipe(sourcemaps.init())
    // sassの前に指定
    .pipe(sassGlob())
    // Sassのコンパイルを実行
    .pipe(sass({outputStyle:'expanded'}))//{outputStyle:'nested, expanded, compact, compressed'}
    // ソースマップファイルの保存先。dest先からのPathで記述
    .pipe(sourcemaps.write('./'))
    // cssフォルダー以下に保存
	.pipe(gulp.dest("./src/assets/css"));
});

//ブラウザシンク設定
gulp.task('serve', function(done) {
  browserSync.init({
    server: {
      baseDir: './src/',
      index: 'index.html',
    },
		ghostMode: false
  });
  done();
});

//ブラウザリロード処理
gulp.task('reload', function(done){
  browserSync.reload();
  done();
});

//watch周りの処理
gulp.task('watch', function(done) {
    //gulp.watch(['./develop/ejs/common/*.ejs', '!node_modules'], ['html']);
	gulp.watch('./**/*.ejs', gulp.series('ejs'));
    gulp.watch('./**/*.scss', gulp.task('sass'));
    gulp.watch('./**/*.html', gulp.task('reload'));
    gulp.watch('./**/*.css', gulp.task('reload'));
    gulp.watch('./**/*.js', gulp.task('reload'));
	gulp.watch('./**/*.php', gulp.task('php'));
})

//HTMLに変換して出力する
gulp.task("ejs", function(done) {
  var json = JSON.parse(fs.readFileSync("./develop/json/meta.json","utf-8"));
  gulp.src(
      //(1)EJSファイルの参照先ディレクトリ名
      //「’!’ + “app/dev/ejs/**/_*.ejs”」の部分で、「_(アンダーバー)で始まるejsファイルは参照し設定を行っています。こうしておくことで、HTMLとして出力したくないファイルにはアンダーバーをつければよくなります
      ["develop/ejs/**/*.ejs",'!' + "develop/ejs/**/_*.ejs"]
  )
      //ビルドエラー時にタスクを停止させない
      .pipe(plumber())
      //jsonの読み込み
      .pipe(ejs({json:json}))
			// タイムスタンプを書き換える
      .pipe(through2.obj((chunk, enc, callback)=>{
        const date = new Date();
        chunk.stat.atime = date;
        chunk.stat.mtime = date;
        callback(null, chunk);
      }))
      //ファイルパス末尾に?revを追加したものにパラメータ付与をする
      .pipe(replace(/\.(js|css|gif|jpg|jpeg|png|svg)\?rev/g, '.$1?rev='+revision))
      //(2).pipe(ejs({}, {ext: '.html'}))に変更することで、.htmlファイルが生成されるようになった。
      .pipe(ejs({}, {ext: '.html'}))
      .pipe(rename({ extname: ".html" }))
      //(1)EJSファイルをHTMLに変換／出力する先のディレクトリ名を入れます。
      .pipe(gulp.dest("./src/"));
     done();
});

// デフォルト処理のコール
gulp.task("default", gulp.series(gulp.parallel('serve', 'watch')));
